<br>
<div class="groups form large-9 medium-8 columns content">
    <?= $this->Form->create($group, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]) ?>
    <fieldset>
        <legend><?= __('Editar Grupo') ?></legend>
        <?php
            echo $this->Form->input('nome', ['label'=>'Nome do Grupo:']);
            //echo $this->Form->input('contacts._ids', ['options' => $contacts]);
        ?>
    </fieldset>
    <div class="form-group submit">
    	<div class="col-sm-offset-6 col-md-offset-4 col-sm-6 col-md-4">
          <?php 
          		echo $this->Form->button('Salvar Grupo',array('class'=>'btn btn-primary'));
          		echo $this->Html->link('Cancelar',array('action'=>'index'),array('class'=>'btn btn-danger'));
          ?>
        </div>
	 </div>
    <?= $this->Form->end() ?>
</div>
