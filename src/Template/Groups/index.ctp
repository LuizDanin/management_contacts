<br>
<div class="groups index large-9 medium-8 columns content">
    <h3><?= __('Gerenciar Grupos:'); echo $quantidade_groups; ?></h3>
    <?php
        $linkDiv = $this->Html->link('Adicionar Grupo <span class="glyphicon glyphicon-plus"></span>', array('action' => 'add'),array('class' => 'btn btn-primary','escape' => false));
        echo $this->Html->div('form-group', $linkDiv);
    ?>
  
  <?php
    echo $this->Form->create($groups, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]);
    
 echo $this->Form->input('Buscar:', ['type' => 'autocomplete', 'id'=>'searchInput']);
 ?>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('nome') ?></th>
                <th><?php echo $this->Paginator->sort('criação') ?></th>
                <th><?php echo $this->Paginator->sort('Ultima Modificação') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody id="fbody">
            <?php foreach ($groups as $group): ?>
            <tr>
                <td><?php echo h($group->nome) ?></td>
                <td><?php echo h($group->created->i18nFormat('dd/MM/Y H:m:s')) ?></td>
                <td><?php echo h($group->modified->i18nFormat('dd/MM/Y H:m:s')) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhe', ['action' => 'view',  $group->id], ['escape' => false, 'class'=>'btn btn-xs btn-primary']) ?>
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', ['action' => 'edit',  $group->id], ['escape' => false, 'class' => 'btn btn-xs btn-success']) ?>
                    <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $group->id], ['class' => 'btn btn-xs btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Contato # {0}?', $group->nome)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
            <?= $this->Paginator->next(__('Proximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php 
    echo $this->Html->script('jquery/jquery');
    echo $this->Html->script('view/Contacts/search'); 
?>
