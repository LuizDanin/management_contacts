<br>
<?php 
    echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span> Voltar',array('action'=>'index'),array('class'=>'btn btn-primary', 'escape' => false));
    echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar',array('action'=>'edit',$group->id),array('class'=>'btn btn-success', 'escape' => false));
?>
<h2>Detalhes do Grupo:</h2>
<div class="groups view large-9 medium-8 columns content">
    <table  class="table table-no-border">
        <tr>
            <th class="col-sm-1 text-left"><?= __('Nome do Grupo:') ?></th>
            <td class="col-sm-9 text-left"><?= h($group->nome) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Data de Criação:') ?></th>
            <td><?= h( $group->created->i18nFormat('dd/MM/Y H:m:s') ) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Ultima Modificação:') ?></th>
            <td><?= h( $group->modified->i18nFormat('dd/MM/Y H:m:s') ) ?></td>
        </tr>
    </table>
    <div class="related">
        <?php 
            $idGrupo = $group->id;
             if (!empty($group->contacts))
             {
                $quantidade_contatos = 0;

                foreach ($group->contacts as $contacts)
                    $quantidade_contatos++;

             }
        ?>
        <h3><?= __('Contatos vinculados a este Grupo ('); echo $quantidade_contatos; ?>):</h3>
        <?php if (!empty($group->contacts)): ?>
        <table  class="table table-no-border">
            <tr>
                <th><?= __('Nome') ?></th>
                <th><?= __('Instituicao') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Data de Criação') ?></th>
                <th><?= __('Ultima Modificação') ?></th>
                <th class="actions"><?= __('Ação') ?></th>
            </tr>
            <?php foreach ($group->contacts as $contacts): ?>
            <tr>
                <td><?= h($contacts->nome) ?></td>
                <td><?= h($contacts->instituicao) ?></td>
                <td><?= h($contacts->email) ?></td>
                <td><?= h($contacts->created->i18nFormat('dd/MM/Y H:m:s') ) ?></td>
                <td><?= h($contacts->modified->i18nFormat('dd/MM/Y H:m:s') ) ?></td>
                <td class="actions">
                     <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhes do Contato', ['controller' => 'Contacts','action' => 'view', $contacts->id], ['escape' => false, 'class'=>'btn btn-xs btn-success']) ?>
                     <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-off"></span> Desvincular Contato'), ['action' => 'desvinculaContatoGrupo', $idGrupo, $contacts->id], ['class' => 'btn btn-xs btn-warning','escape' => false, 'confirm' => __('Deseja Desvincular este Contato do Grupo # {0}?', $group->nome)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
