<div class="contacts form large-9 medium-8 columns content">
    <?= $this->Form->create($contact, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]) ?>
    <fieldset>
        <legend><?= __('Editar Contato') ?></legend>
        <?php
            echo $this->Form->input('nome', ['label'=>'Nome:']);
            echo $this->Form->input('instituicao');
            echo $this->Form->input('email');
            echo $this->Form->input('email_alternativo');
            echo $this->Form->input('telefone');
            echo $this->Form->input('telefone_alternativo');
            echo $this->Form->input('groups._ids', ['options' => $groups]);
            //echo $this->Form->input('group_id');
        ?>
    </fieldset>
	<div class="form-group submit">
		<div class="col-sm-offset-6 col-md-offset-4 col-sm-6 col-md-4">
	      <?php
							echo $this->Form->button ( 'Salvar Contato', array (
									'class' => 'btn btn-primary' 
							) );
							
							echo $this->Html->link ( 'Cancelar', array (
									'action' => 'index' 
							), array (
									'class' => 'btn btn-danger' 
							) );
		?>
       </div>
	</div>
    <?php echo $this->Form->end() ?>
</div>
