<?php echo $this->Form->create($contact, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]) ?>
    <fieldset>
        <legend><?= __('Cadastrar Contatos') ?></legend>
        <?php
            echo $this->Form->input('nome', ['label'=>'Nome:']);
            echo $this->Form->input('instituicao', ['label'=>'Instituição:']);
            echo $this->Form->input('email', ['label'=>'E-mail:']);
            echo $this->Form->input('email_alternativo', ['label'=>'E-mail Alternativo:']);
            echo $this->Form->input('telefone', ['label'=>'Telefone Pessoal:']);
            echo $this->Form->input('telefone_alternativo', ['label'=>'Telefone Institucional:']);
            echo $this->Form->input('groups._ids', ['label'=>'Selecione os Grupos:','options' => $groups]);
        ?>
    </fieldset>
    <div class="form-group submit">
	<div class="col-sm-offset-6 col-md-offset-4 col-sm-6 col-md-4">
      <?php 
      		echo $this->Form->button('Salvar Contato',array('class'=>'btn btn-primary'));
      		echo $this->Html->link('Cancelar',array('action'=>'index'),array('class'=>'btn btn-danger'));
      ?>
    </div>
</div>
    <?php //echo $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>