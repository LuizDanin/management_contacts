<br><br>
<?php 
//$id = $this->params['pass'][0];

    echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span> Voltar',array('action'=>'index'),array('class'=>'btn btn-primary', 'escape' => false));

    echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar',array('action'=>'edit',$contact->id), array('class'=>'btn btn-success', 'escape' => false));

    echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $contact->id], ['class' => 'btn btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Contato # {0}?', $contact->nome)]);
?>
<h2>Detalhes do Contato:</h2>
<div class="contacts view large-9 medium-8 columns content">
    <table class="table table-no-border">
        <tr>
            <th class="col-sm-1 text-left"><?php echo __('Nome do Contato:') ?></th>
            <td  class="col-sm-9 text-left"><?php echo h($contact->nome) ?></td>
        </tr>
        <tr>
            <th  class="text-left"><?php echo __('Instituicao:') ?></th>
            <td><?php echo h($contact->instituicao) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Email:') ?></th>
            <td><?= h($contact->email) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Email Alternativo:') ?></th>
            <td><?= h($contact->email_alternativo) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Telefone Pessoal:') ?></th>
            <td><?= h($contact->telefone) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Telefone Institucional:') ?></th>
            <td><?= h($contact->telefone_alternativo) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Data de Criação:') ?></th>
            <td><?= h($contact->created->i18nFormat('dd/MM/Y H:m:s')) ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= __('Ultima de Modificação:') ?></th>
            <td><?= h($contact->modified->i18nFormat('dd/MM/Y H:m:s')) ?></td>
        </tr>
    </table>
    <div class="related">

    <?php 
        if ( !empty($contact->groups) )
        {
            $quantidade_grupos = 0;
            foreach ($contact->groups as $group)
            {
                $quantidade_grupos++;
            }
        }
    ?>

        <h3><?= __('Grupos Vinculados ('); echo $quantidade_grupos ?>):</h3>
        <?php if (!empty($contact->groups)): ?>
        <table class="table table-no-border">
            <tr>
                <th><?= __('Nome') ?></th>
                <th><?= __('Data de Criação') ?></th>
                <th><?= __('Ultima de Modificação') ?></th>
                <th class="actions"><?= __('Ação') ?></th>
            </tr>
            <?php foreach ($contact->groups as $groups): ?>
            <tr>
                <td><?= h($groups->nome) ?></td>
                <td><?= h($groups->created->i18nFormat('dd/MM/Y H:m:s')) ?></td>
                <td><?= h($groups->modified->i18nFormat('dd/MM/Y H:m:s')) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhes do Grupo', ['controller' => 'Groups','action' => 'view', $groups->id], ['escape' => false, 'class'=>'btn btn-xs btn-success']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
