<br>
<div class="contacts index large-9 medium-8 columns content">
    <h3><?= __('Gerenciar Contatos:') ?> <?php echo $quantidade_contatos; ?></h3>
<?php
    $linkDiv = $this->Html->link('Adicionar Contato <span class="glyphicon glyphicon-plus"></span>', array('action' => 'add'),array('class' => 'btn btn-primary','escape' => false));
    echo $this->Html->div('form-group', $linkDiv);
?>

<?php
    echo $this->Form->create($contacts, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]);
    
    echo $this->Form->input('Buscar:', ['type' => 'autocomplete', 'id'=>'searchInput']); 
?>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('instituicao') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody id="fbody">
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?= h($contact->nome) ?></td>
                <td><?= h($contact->instituicao) ?></td>
                <td><?= h($contact->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhe', ['action' => 'view', $contact->id], ['escape' => false, 'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', ['action' => 'edit', $contact->id], ['escape' => false, 'class' => 'btn btn-xs btn-success']) ?>
                    <?= $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $contact->id], ['class' => 'btn btn-xs btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Contato # {0}?', $contact->nome)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
            <?= $this->Paginator->next(__('Proximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?php 
    echo $this->Html->script('jquery/jquery');
    echo $this->Html->script('view/Contacts/search'); 
?>