<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $groups = $this->paginate($this->Groups->find('all', [  'order' => ['nome' => 'ASC'] ] ) );
        $groups_todos = $this->Groups->find('all', [  'order' => ['nome' => 'ASC'] ] );


        $this->set(compact('groups'));
        $this->set('_serialize', ['groups']);

        $quantidade_groups = 0;
         foreach ($groups_todos as $groups)
         {
            $quantidade_groups++;
         }

        $this->set(compact('quantidade_groups'));
        $this->set('_serialize', ['quantidade_groups']);
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Contacts' => function ($q) {
        return $q->order(['nome' =>'ASC']);
    }]
        ]);
        
        $this->set('group', $group);
        $this->set('_serialize', ['group']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('O grupo foi Salvo com sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo não pode ser salvo. Por favor, tente novamente.'));
            }
        }
        $contacts = $this->Groups->Contacts->find('list', ['limit' => 200]);
        $this->set(compact('group', 'contacts'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Contacts']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('O grupo foi Editado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo não pode ser Editado. Por favor, tente novamente.'));
            }
        }
        $contacts = $this->Groups->Contacts->find('list', ['limit' => 200]);
        $this->set(compact('group', 'contacts'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);
        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('O grupo foi excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Não foi possível excluir o grupo. Por favor, tente novamente.') );
        }
        return $this->redirect(['action' => 'index']);
    }

    public function desvinculaContatoGrupo($idGrupo = null, $idContato = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if ( !empty($idContato) && !empty($idGrupo) )
        {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $conn->execute('DELETE FROM contacts_groups WHERE contact_id = ? AND group_id = ?', [$idContato, $idGrupo]);
            $conn->commit();
            
            $this->Flash->success(__('O contato foi desvinculado do Grupo com sucesso.'));
            return $this->redirect( ['action' => 'view',$idGrupo]);
        }
    }
    
    public function comunicate()
    {
        
    }
}