<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 */
class ContactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $contacts = $this->paginate( $this->Contacts->find('all', [  'order' => ['nome' => 'ASC'] ] ) );

        $contacts_todos =  $this->Contacts->find('all', [  'order' => ['nome' => 'ASC'] ] );

        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);

        $quantidade_contatos = 0;
         foreach ($contacts_todos as $contact)
         {
            $quantidade_contatos++;
         }

        $this->set(compact('quantidade_contatos'));
        $this->set('_serialize', ['quantidade_contatos']);
    }

    /**
     * View method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => ['Groups' => function ($q) {
        return $q->order(['nome' =>'ASC']);
    }]
        ]);

        $this->set('contact', $contact);
        $this->set('_serialize', ['contact']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            
            /* echo "<pre>";
            var_dump($this->request->data);
            echo "</pre>";
            die(); */


            
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);

            //var_dump($contact);
            //die;

            if ($this->Contacts->save($contact))
            {
                $this->Flash->success(__('O contato foi Salvo com Sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contato não pode ser salvo. Por favor, tente novamente.'));
            }
        }
        
        $groups = $this->Contacts->Groups->find('all')->select(['id', 'nome'])->order(['nome'=>'ASC']);
        
        $arrayGroup = array();
        foreach ($groups as $group)
        {
        	$arrayGroup[ $group['id'] ] = $group['nome'];
        }
        
        $groups = $arrayGroup;
        $this->set(compact('contact', 'groups'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => ['Groups']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('O contato foi Editado com Sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contato não pode ser salvo. Por favor, tente novamente.'));
            }
        }
        
        $groups = $this->Contacts->Groups->find('all')->select(['id', 'nome'])->where($id)->order(['nome'=>'ASC']);
        
        $arrayGroup = array();
        foreach ($groups as $group)
        {
        	$arrayGroup[ $group['id'] ] = $group['nome'];
        }
        $groups = $arrayGroup;
        
        $this->set(compact('contact', 'groups'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contact = $this->Contacts->get($id);
        if ($this->Contacts->delete($contact)) {
            $this->Flash->success(__('O contato foi Removido com Sucesso.'));
        } else {
            $this->Flash->error(__('Não foi possível excluir o contato. Por favor, tnte novamente.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
