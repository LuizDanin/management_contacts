<?php
use Cake\Core\Configure;

/**
 * Default `html` block.
 */
if (!$this->fetch('html')) {
    $this->start('html');
    printf('<html lang="%s" class="no-js">', Configure::read('App.language'));
    $this->end();
}

/**
 * Default `title` block.
 */
if (!$this->fetch('title')) {
    $this->start('title');
    echo Configure::read('App.title');
    $this->end();
}

/**
 * Default `footer` block.
 */
if (!$this->fetch('tb_footer')) {
    $this->start('tb_footer');
    printf('&copy;%s %s', date('Y'), Configure::read('App.title'));
    $this->end();
}

/**
 * Default `body` block.
 */
$this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
if (!$this->fetch('tb_body_start')) {
    $this->start('tb_body_start');
    echo '<body' . $this->fetch('tb_body_attrs') . '>';
    $this->end();
}
/**
 * Default `flash` block.
 */
if (!$this->fetch('tb_flash')) {
    $this->start('tb_flash');
    if (isset($this->Flash))
        echo $this->Flash->render();
    $this->end();
}
if (!$this->fetch('tb_body_end')) {
    $this->start('tb_body_end');
    echo '</body>';
    $this->end();
}

/**
 * Prepend `meta` block with `author` and `favicon`.
 */
$this->prepend('meta', $this->Html->meta('author', null, ['name' => 'author', 'content' => Configure::read('App.author')]));
$this->prepend('meta', $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']));

/**
 * Prepend `css` block with Bootstrap stylesheets and append
 * the `$html5Shim`.
 */
$html5Shim =
<<<HTML
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
HTML;
$this->prepend('css', $this->Html->css(['bootstrap/bootstrap.min']) );
$this->prepend('css', $this->Html->css(['bootstrap/font-awesome.min']) );
$this->prepend('css', $this->Html->css(['general']) );
$this->prepend('css', $this->Html->css(['menu/sm-core-css']) );
$this->prepend('css', $this->Html->css(['menu/sm-mint']) );

$this->append('css', $html5Shim);

/**
 * Prepend `script` block with jQuery and Bootstrap scripts
 */
$this->prepend('script', $this->Html->script(['jquery/jquery.min', 'bootstrap/bootstrap','menu/jquery.smartmenus.min']));

?>
<!DOCTYPE html>

<?= $this->fetch('html') ?>

    <head>

        <?= $this->Html->charset() ?>

        <title><?= $this->fetch('title') ?></title>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        
    </head>
    <br>
<nav role="navigation">
  <!-- Sample menu definition -->
  <ul id="main-menu" class="sm sm-mint">
    <li><?= $this->Html->link(__('Início'), ['controller' => 'Pages','action' => 'display']) ?></li>
    <li><a href="#">Gerencia</a>
      <ul>
        <li><?= $this->Html->link(__('Contatos'), ['controller' => 'Contacts','action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Grupos'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <!-- <li><a href="http://www.smartmenus.org/about/vadikom/">The company</a>
          <ul>
            <li><a href="http://vadikom.com/about/">About Vadikom</a></li>
            <li><a href="http://vadikom.com/projects/">Projects</a></li>
            <li><a href="http://vadikom.com/services/">Services</a></li>
            <li><a href="http://www.smartmenus.org/about/vadikom/privacy-policy/">Privacy policy</a></li>
          </ul>
        </li> -->
      </ul>
    </li>
    <!-- S<li><?php //echo $this->Html->link(__('Comunicar Grupos'), ['controller' => 'Groups', 'action' => 'comunicate']) ?></li> -->
    
    <li><a href="#">Sobre</a>
      <ul class="mega-menu">
        <li>
          <!-- The mega drop down contents -->
          <div style="width:400px;max-width:100%;">
            <div style="padding:5px 24px;">
              <p>Esta é uma aplicação Web para agenda de contatos que possui o propósito de auxiliar na gestão das informações dos contatos no Gmail do Projeto SINTREGRA.</p>
              <p>A expectativa dos desenvolvedores que idealizaram e implementaram esta simples agenda de contatos é que ela seja util agregando valor e produtividade nas atividades fim do <strong>Projeto SINTEGRA</strong>.</p>
  	  </div>
  	</div>
        </li>
      </ul>
    </li>
  </ul>
</nav>
    <?php
    echo $this->fetch('tb_body_start');
    echo $this->fetch('tb_flash');
    echo $this->fetch('content');
    echo $this->fetch('tb_footer');
    echo $this->fetch('script');
    echo $this->fetch('tb_body_end');
    ?>
    <script type="text/javascript">
    $(function() {
    	$('#main-menu').smartmenus({
    		subMenusSubOffsetX: 6,
    		subMenusSubOffsetY: -8
    	});
    });
    </script>
</html>